#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <time.h>

#include <pthread.h>

#include "primitives.h"
#include "raytracing.h"

#define OUT_FILENAME "out.ppm"

#define ROWS 512
#define COLS 512

static void write_to_ppm(FILE *outfile, uint8_t *pixels,
                         int width, int height)
{
    fprintf(outfile, "P6\n%d %d\n%d\n", width, height, 255);
    fwrite(pixels, 1, height * width * 3, outfile);
}

static double diff_in_second(struct timespec t1, struct timespec t2)
{
    struct timespec diff;
    if (t2.tv_nsec-t1.tv_nsec < 0) {
        diff.tv_sec  = t2.tv_sec - t1.tv_sec - 1;
        diff.tv_nsec = t2.tv_nsec - t1.tv_nsec + 1000000000;
    } else {
        diff.tv_sec  = t2.tv_sec - t1.tv_sec;
        diff.tv_nsec = t2.tv_nsec - t1.tv_nsec;
    }
    return (diff.tv_sec + diff.tv_nsec / 1000000000.0);
}

int main(int argc, char* argv[])
{
    uint8_t *pixels;
    light_node lights = NULL;
    rectangular_node rectangulars = NULL;
    sphere_node spheres = NULL;
    color background = { 0.0, 0.1, 0.1 };
    struct timespec start, end;

#include "use-models.h"

    /* allocate by the given resolution */
    pixels = malloc(sizeof(unsigned char) * ROWS * COLS * 3);
    if (!pixels) exit(-1);

    printf("# Rendering scene\n");
    /* do the ray tracing with the given geometry */
    clock_gettime(CLOCK_REALTIME, &start);
    //TODO
    /*This is a domain function for this program.
     * Please trace the parameter of it and create your threads to do the function*/
    struct raytracing_param data;
    data.pixels = pixels;
    memcpy(data.background_color, background, sizeof(background));
    data.rectangulars = rectangulars;
    data.spheres = spheres;
    data.lights = lights;
    data.view = &view;
    data.width = ROWS;
    data.height = COLS;
    data.h0 = 0;
    data.h1 = data.height;
    data.core_id = -1;
    if (argc == 1) {
        raytracing(pixels, background,
                   rectangulars, spheres, lights, &view, ROWS, COLS);
    } else if (strcmp(argv[1], "1") == 0) {
        pthread_t tid;
        int status;

        if (pthread_create(&tid, NULL, (void *)pthread_raytracing, &data) != 0)
            fprintf(stderr, "pthread_create error");
        pthread_join(tid, (void*) &status);
    } else if (strcmp(argv[1], "2") == 0) {
        int thread_num = 2;
        if (argc == 3) thread_num = atoi(argv[2]);
        struct raytracing_param datas[thread_num];
        pthread_t tid[thread_num];
        int status[thread_num];

        int height_split = data.height / thread_num;
        for(int i = 0; i < thread_num; i++) {
            datas[i] = data;
            datas[i].h0 = height_split * i;
            datas[i].h1 = height_split * (i+1);
            datas[i].core_id = i;
        }
        datas[thread_num].h1 = data.height;

        for(int i = 0; i < thread_num; i++)
            if (pthread_create(&tid[i], NULL, (void *)pthread_raytracing, &datas[i]) != 0)
                fprintf(stderr, "pthread_create error");
        for(int i = 0; i < thread_num; i++)
            pthread_join(tid[i], (void*) &status[i]);
    } else if (strcmp(argv[1], "3") == 0) {
        int thread_num = 2;
        if (argc == 3) thread_num = atoi(argv[2]);
        struct raytracing_param datas[thread_num];
        pthread_t tid[thread_num];
        int status[thread_num];

        int height_split = data.height / thread_num;
        for(int i = 0; i < thread_num; i++) {
            datas[i] = data;
            datas[i].h0 = height_split * i;
            datas[i].h1 = height_split * (i+1);
            datas[i].core_id = 0;
        }
        datas[thread_num].h1 = data.height;

        for(int i = 0; i < thread_num; i++)
            if (pthread_create(&tid[i], NULL, (void *)pthread_raytracing, &datas[i]) != 0)
                fprintf(stderr, "pthread_create error");
        for(int i = 0; i < thread_num; i++)
            pthread_join(tid[i], (void*) &status[i]);
    } else {
    }
    clock_gettime(CLOCK_REALTIME, &end);
    {
        FILE *outfile = fopen(OUT_FILENAME, "wb");
        write_to_ppm(outfile, pixels, ROWS, COLS);
        fclose(outfile);
    }

    delete_rectangular_list(&rectangulars);
    delete_sphere_list(&spheres);
    delete_light_list(&lights);
    free(pixels);
    printf("Done!\n");
    printf("Execution time of raytracing() : %lf sec\n", diff_in_second(start, end));
    return 0;
}
