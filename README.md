# Homework 1-1 (Multithread)

## Description
我使用 `argv[1]` 去判斷要跑哪一種模式。

### 1. n 個 core 輪流跑同一個 thread

`./raytracing 1`

![1 thread 8 cores](images/2dPNVoa.png)

利用 `pthread_setaffinity_np` 去指定現在要在哪一個 core 上執行。

為了將參數傳入 function ，所以我新增了一個 struct `raytracing_param` 來傳遞參數。

### 2. n 個 core 跑 n 個 thread

`./raytracing 2 <n>`

![4 threads 4 cores](images/VGD8hbY.png)

將圖片的高度分成 n 等分，讓不同 thread 分別執行，因此需要記錄起始和結束的圖片縱座標 (`h0`, `h1`)，此外也會記錄要在哪一個 core 上執行。

### 3. 1 個 core 跑 n 個 thread

`./raytracing 3 <n>`

利用上一題所寫的程式，並把要運行的 core id 都設成同一個即可。

## Analysis

以下針對不同 thread 數量進行測試，每一項的時間為執行五次後的平均值。

平台：

* Fedora 22 x86_64 (linux 4.4.14)
* Intel i7-4770MQ 2.4GHz (Turbo 3.4GHz) 4T8C
* 8GB RAM

### Experiments

![](images/41CuvEh.png)

可以發現 1 core 8 threads 的執行時間，反而比 1 core 1 thread 的時間還久一點，我推測是因為 CPU 還需要做 context switch 造成的。

### Speed up

![](images/jvwoxKP.png)

由於只有第二種方式有實際提升的效果，所以只針對此方法做圖。

可以看出在 2 threads 的情況下提升了將近兩倍，是很合理的。但到 8 threads 時卻只有 3.9 倍，這是因為 Intel HT (Hyper Threading) 技術才讓 OS 看起來有 8 個 thread，實際上只有 4 個物理核心。

此外我刻意讓兩個 thread 跑在 CPU0、CPU4 ，效能提升會大打折扣，這也是因為 HT 的關係，其實它們兩顆是同一個核心，共享 L1, L2 cache 。

## Bonus

第二、三小題，都可以透過最後一個參數，去指定 thread 數量。

若第二小題的 thread 數量超過 core 數量，也會印出錯誤訊息。
